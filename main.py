""" This program calculates the optimal decision for an intersection given a set of data"""
import csv
import itertools
from collections import Counter


flow = ['H', 'L']
lights = ['N', 'W', 'E']
goal = flow[1] + flow[1] + flow[1]


def check(keys) -> None:
    """This checks if the received data is expected"""
    for key in keys:
        try:
            if any(symbol not in flow for symbol in key[:3]):
                raise ValueError('Wrong flow value at the beginning state')
            if key[3] not in lights:
                raise ValueError('Wrong light value')
            if any(symbol not in flow for symbol in key[4:7]):
                raise ValueError('Wrong flow value at the end state')
            if len(key) != 7:
                raise ValueError('Received data does not have the correct length')
        except IndexError as exception:
            raise ValueError('Wrong data format') from exception


with open('Data.csv', encoding="utf-8") as csv_file:
    occurrences = dict(Counter(data := [''.join([c[0] for c in line]) for line in list(csv.reader(csv_file, delimiter=';'))[1:]]))
check(occurrences)

transitions = {key[:4]: sum(occurrences[key[:4] + x + y + z] for x, y, z in itertools.product(flow, flow, flow) if key[:4]+x+y+z in occurrences) for key in occurrences}

prob_dict = {event: repetition / transitions[event[:4]] for event, repetition in occurrences.items()}

costsI = costs = {a+b+c: 0 for a, b, c in itertools.product(flow, flow, flow)}
while all(costs[state] - costsI[state] > 0.001 for state in costs if state != goal) or any(val == 0 for key, val in costs.items() if key != goal):
    costsI = costs.copy()
    costs = {state: min([1 + sum(costsI[nextS] * prob_dict[state + T + nextS] for nextS in costs if state + T + nextS in occurrences) for T in lights]) for state in costs if state != goal}

letter = {state: "N" if (i := min(range(3), key=[1 + sum(costsI[nextS] * prob_dict[state + T + nextS] for nextS in costs if state + T + nextS in occurrences) for T in lights].__getitem__)) == 0 else "W" if i == 1 else "E" for state in costs if state != goal}


# print(data)
print(len(data), " lines of data have been read")
print("There are ", len(occurrences), " different events, the events and their occurrences are as follows:")
print(occurrences)
print("\nThere are ", len(transitions), " different initial states and transitions, these and the times they occur are:")
print(transitions)
print("\nThe probability of ending in a state, given an initial state and a light, is as follows:")
print(prob_dict)
print("\nThe costs of the possible states, and the optimal path are as follows:")
print(costs)
print(letter)

# print(occurrences["HHHEHLH"]) # Examples of usage
# print(prob_dict["HHHEHLH"])
